#include <iostream>
#include "triangulo.hpp"

Triangulo::Triangulo(){
	setLados(3);
	setLargura(10);
	setAltura(15);
}
Triangulo::Triangulo(float largura, float altura){
	setLados(3);
	setLargura(largura);
	setAltura(altura);
}

float Triangulo::calculaArea(){
	setArea(getLargura() * getAltura() / 2.0f);
	return getArea();
}
float Triangulo::calculaPerimetro(){
	setPerimetro(3 * getLargura());
	return getPerimetro();
}
