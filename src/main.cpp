#include <iostream>
#include "quadrado.hpp"
#include "triangulo.hpp"

using namespace std;

int main() {

	Quadrado * forma1 = new Quadrado(50.0);
	Triangulo * forma2 = new Triangulo(11, 30);

	forma1->calculaArea();
	forma1->calculaPerimetro();
	
	forma2->calculaArea();
	forma2-> calculaPerimetro();

	cout << "Area do Quadrado: " << forma1->getArea() << endl;
	cout << "Perimetro do Quadrado: " << forma1->getPerimetro() << endl;

	cout << "Area do Triangulo: " << forma2->getArea() << endl;
	cout << "Perimetro do Triangulo: " <<forma2->getPerimetro() << endl;
	
	delete(forma1);
	delete(forma2);
}
