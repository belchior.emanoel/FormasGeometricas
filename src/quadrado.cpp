#include <iostream>
#include "quadrado.hpp"

Quadrado::Quadrado(){
	setLados(4);
	setLargura(0);
	setAltura(0);
}
Quadrado::Quadrado(float largura){
	setLados(4);
	setLargura(largura);
	setAltura(largura);
}
