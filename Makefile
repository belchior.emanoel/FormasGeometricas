BINFOLDER := ./bin/
INCFOLDER := ./inc/
SRCFOLDER := ./src/
OBJFOLDER := ./obj/

COMPILER := g++
FLAGS := -Wall -ansi -I$(INCFOLDER)

SRCFILES := $(wildcard src/*.cpp)

all:	 $(SRCFILES:src/%.cpp=obj/%.o)
	$(COMPILER) $(FLAGS) $(OBJFOLDER)*.o -o $(BINFOLDER)formasgeometricas

obj/%.o:	src/%.cpp
				$(COMPILER) $(FLAGS) -c $< -o $@

.PHONY: clean
clean:
	rm -rf $(OBJFOLDER)*.o

run:
	$(BINFOLDER)formasgeometricas

