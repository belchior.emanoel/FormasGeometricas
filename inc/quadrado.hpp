#ifndef QUADRADO_H
#define QUADRADO_H

#include "formaGeometrica.hpp"

class Quadrado : public FormaGeometrica {

	public:
		Quadrado();
		Quadrado(float largura);
};
#endif
