#ifndef TRIANGULO_H
#define TRIANGULO_H

#include "formaGeometrica.hpp"

class Triangulo : public FormaGeometrica {

	public:
		Triangulo();
		Triangulo(float largura, float altura);
	
		float calculaArea();
		float calculaPerimetro();

};
#endif 
