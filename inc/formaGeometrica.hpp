#ifndef FORMAGEOMETRICA_H
#define FORMAGEOMETRICA_H

class FormaGeometrica {


private:
	int lados;
	float altura;
	float largura;
	float area;
	float perimetro;
public:
	FormaGeometrica();
	FormaGeometrica(float altura, float largura);
	void setAltura(float altura);
	float getAltura();
	void setLargura(float largura);
	float getLargura();
	void setLados(int lados);
	int getLados();
	float getArea();
	float getPerimetro();
	float calculaArea();
	float calculaPerimetro();
protected:
	void setArea(float area);
	void setPerimetro(float perimetro);
};

#endif
